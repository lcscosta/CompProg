-- import Data.List

myQuad :: Float -> Float -> String
myQuad x1 y1
    | (x1 >0 && y1 > 0) = "Q1"
    | (x1 > 0 && y1 < 0) = "Q4"
    | (x1 < 0 && y1 > 0) = "Q2"
    | (x1 < 0 && y1 < 0) = "Q3"
    | (x1 == y1 && x1 == 0) = "Origem"

toSplit :: String -> [String]
toSplit = splitWords . dropWhile (==' ') 
    where
        splitWords "" = []
        splitWords s = 
          let word = takeWhile (/=' ') s
              (_, rest) = splitAt (length word) s
          in word : splitWords (dropWhile (==' ') rest)

main :: IO()
main = do
    phrase <- getLine
    let xy = toSplit phrase
    let x = (read (xy!!0) :: Float)
    let y = (read (xy!!1) :: Float)
    
    putStrLn (myQuad x y)
